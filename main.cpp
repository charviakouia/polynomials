
#include <cstdlib>
#include <iostream>
#include "poly.h"

int main(int argc, char* argv[]){
    Polynomial x(2, -7, 2, 5);
    std::cout << "Original: " << x;
    x.verticalFactorShift(4);
    std::cout << "After upwards shift: " << x;
    x.verticalFactorStretch(2);
    std::cout << "After vertical stretch: " << x;
    x.horizontalFactorStretch(0.2);
    std::cout << "After sideways stretch: " << x;
    x.horizontalFactorShift(-5);
    std::cout << "After rightwards shift: " << x;
    return 0;
}