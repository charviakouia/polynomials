
#include <cstdarg>
#include <sstream>
#include <cstdlib>
#include <iostream>
#include <cmath>
#include "poly.h"
#include "math.h"

Polynomial::Polynomial(int nOrder, ...) : nOrder(nOrder){
    va_list list;
    pdVariables = new double[nOrder + 1];
    va_start(list, nOrder);
    for (int i = 0; i <= nOrder; i++){
        pdVariables[i] = static_cast<double>(va_arg(list, int));
    }
    va_end(list);
}

std::ostream& operator<<(std::ostream &out, Polynomial &poly){
    out << poly.toString();
    return out;
}

void Polynomial::horizontalFactorStretch(double nFactor){
    for(int i = 0; i <= nOrder; i++){
        pdVariables[i] /= pow(nFactor, nOrder - i);
    }
}

void Polynomial::verticalFactorShift(double nFactor){
    pdVariables[nOrder] += nFactor;
}

void Polynomial::verticalFactorStretch(double nFactor){
    for(int i = 0; i <= nOrder; i++){
        pdVariables[i] *= nFactor;
    }
}

void Polynomial::horizontalFactorShift(double nFactor){
    for (int i = nOrder - 1; i >= 0; i--){
        double dMainFactor = pdVariables[i];
        for (int ii = i + 1; ii <= nOrder; ii++){
            pdVariables[ii] += dMainFactor * pow(-nFactor, ii - i) * choose(nOrder - i, ii - i);
        }
    }
}

std::string Polynomial::toString(){
    std::stringstream ss;
    ss << pdVariables[0];
    if (nOrder < 1){
        ss << '\n';
        return ss.str();
    } else if (nOrder == 1) {
        ss << " * x";
    } else {
        ss << " * x^" << nOrder;
    }
    for (int i = 1; i < nOrder; i++){
        int nCurrent = pdVariables[i];
        if (nCurrent == 0){
            continue;
        } else if (i == nOrder - 1){
            ss << ((nCurrent < 0) ? " - " : " + ");
            ss << std::abs(nCurrent) << " * x";
            break;
        }
        ss << ((nCurrent < 0) ? " - " : " + ");
        ss << std::abs(nCurrent) << " * x^" << nOrder - i;
    }
    ss << ((pdVariables[nOrder] < 0) ? " - " : " + ");
    ss << std::abs(pdVariables[nOrder]) << '\n';
    return ss.str();
}