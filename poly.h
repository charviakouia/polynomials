
#ifndef POLY_H
#define POLY_H

class Polynomial {
    private:
        int nOrder;
        double* pdVariables;
    public:
        Polynomial(int nOrder, ...);
        void horizontalFactorShift(double nFactor);
        void horizontalFactorStretch(double nFactor);
        void verticalFactorShift(double nFactor);
        void verticalFactorStretch(double nFactor);
        std::string toString();
        friend std::ostream& operator<<(std::ostream &out, Polynomial &poly);
};

#endif