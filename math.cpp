
#include "math.h"

int choose(int n, int m){
    if (n < m){
        return 0;
    }
    int r = 1;
    for (int i = 1; i <= m; i++){
        r *= n--;
        r /= i;
    }
    return r;
}